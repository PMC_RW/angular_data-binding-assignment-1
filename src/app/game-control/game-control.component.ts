import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css'],
})
export class GameControlComponent {
  @Output() onStartGame = new EventEmitter<MouseEvent>();
  @Output() onStopGame = new EventEmitter<MouseEvent>();
  isGameActive: boolean = false;

  handleStartButtonClick(event: MouseEvent) {
    this.onStartGame.emit(event);
    this.isGameActive = true;
  }

  //_________________________________________________________________________

  // @Output() intervalFired = new EventEmitter<number>();
  // interval;
  // lastNumber = 0;

  // handleStartButtonClick() {
  //   this.interval = setInterval(() => {
  //     this.onStartGame.emit(this.lastNumber + 1);
  //     this.lastNumber++
  //   }, 1000)
  // }

  //_________________________________________________________________________

  handleStopButtonClick(event: MouseEvent) {
    this.onStopGame.emit(event);
    this.isGameActive = false;
  }
}
