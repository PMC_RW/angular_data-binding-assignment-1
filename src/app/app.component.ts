import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'cmp-databinding-assignment-1';

  count: number = 0;
  oddNumbers: number[] = [];
  evenNumbers: number[] = [];
  gameNumbers: number[] = [];
  gameInterval: any;

  gameLogic() {
    this.count++;
    this.gameNumbers.push(this.count);
    if (this.count % 2 === 0) {
      this.evenNumbers.push(this.count);
    } else {
      this.oddNumbers.push(this.count);
    }
  }

  handleGameStarted(): void {
    this.gameInterval = setInterval(() => this.gameLogic(), 1000);
  }

  handleGameStopped() {
    clearInterval(this.gameInterval);
  }

  checkIsOdd(number: number): string {
    if (number % 2 === 0) {
      return 'green';
    } else {
      return 'red';
    }
  }

  // for custom structural directive "unless"
  condition: boolean = false;
  handleToggleButton(): void {
    this.condition = !this.condition;
  }

  // for ngSwitch
  value: number = 5;
}
